from flask_login import UserMixin, AnonymousUserMixin, LoginManager, login_user, logout_user, login_required, current_user
from flask import Flask, render_template, url_for, flash, redirect, request, Blueprint, abort
from . import main
from ..models import User, Post
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo
from wtforms import ValidationError
from .. import db
from .email import send_email
import logging

class LoginForm(FlaskForm):
	email = StringField('Email', validators=[DataRequired(), Length(1, 64), Email()])
	password = PasswordField('Password', validators=[DataRequired()])
	remember_me = BooleanField('Keep me logged in')
	submit = SubmitField('Log In')

class RegistrationForm(FlaskForm):
	email = StringField('Email', validators=[DataRequired(), Length(1, 64), Email()])
	username = StringField('Username', validators=[
		DataRequired(), Length(1, 64),
		Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
			'Usernames must have only letters, numbers, dots or '
			'underscores')])
	password = PasswordField('Password', validators=[
		DataRequired(), EqualTo('password2', message='Passwords must match.')])
	password2 = PasswordField('Confirm password', validators=[DataRequired()])
	submit = SubmitField('Register')

	def validate_email(self, field):
		if User.query.filter_by(email=field.data.lower()).first():
			raise ValidationError('Email already registered.')

	def validate_username(self, field):
		if User.query.filter_by(username=field.data).first():
			raise ValidationError('Username already in use.')

class PostForm(FlaskForm):
	title = StringField('Title', validators=[DataRequired(), Length(1, 256)])
	body = TextAreaField("Text", validators=[DataRequired()])
	submit = SubmitField('Submit')

class DeleteUserForm(FlaskForm):
	submit = SubmitField('Confirm deletion')

@main.route('/register', methods=['GET', 'POST'])
def register():
	form = RegistrationForm()
	if form.validate_on_submit():
		user = User(email=form.email.data.lower(),
					username=form.username.data,
					password=form.password.data)
		db.session.add(user)
		db.session.commit()
		token = user.generate_confirmation_token()
		send_email(user.email, 'Confirm Your Account',
				'email-template', user=user, token=token)
		flash('A confirmation email has been sent to you by email.')
		return redirect(url_for('main.login'))
	return render_template('register.html', form=form)

@main.route('/confirm/<token>')
@login_required
def confirm(token):
	if current_user.confirmed:
		return redirect(url_for('main.index'))
	if current_user.confirm(token):
		db.session.commit()
		flash('You have confirmed your account. Thanks!')
	else:
		flash('The confirmation link is invalid or has expired.')
	return redirect(url_for('main.index'))

@main.route('/confirm', methods=['GET', 'POST'])
@login_required
def resend_confirmation():
	token = current_user.generate_confirmation_token()
	send_email(current_user.email, 'Confirm Your Account',
			'email-template', user=current_user, token=token)
	flash('A new confirmation email has been sent to you by email.')
	return redirect(url_for('main.index'))

@main.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data.lower()).first()
		if user is not None and user.verify_password(form.password.data):
			login_user(user, form.remember_me.data)
			next = request.args.get('next')
			if next is None or not next.startswith('/'):
				next = url_for('main.index')
			return redirect(next)
		flash('Invalid email or password.')
	return render_template('login.html', form=form)

@main.route('/logout')
@login_required
def logout():
	logout_user()
	flash('You have been logged out.')
	return redirect(url_for('main.index'))

@main.route('/', methods = ['GET', 'POST'])
def index():
	form = PostForm()
	if not isinstance(current_user, AnonymousUserMixin) and \
			current_user.confirmed and form.validate_on_submit():
		logging.info("Post by %s", current_user)
		post = Post(title = form.title.data,
					body = form.body.data,
					author = current_user._get_current_object())
		db.session.add(post)
		db.session.commit()
		return redirect(url_for('.index'))
	posts = Post.query.order_by(Post.timestamp.desc()).all()
	return render_template('index.html', form=form, posts=posts)

@main.route('/post/<int:id>')
def post(id):
	post = Post.query.get_or_404(id)
	return render_template('post.html', post=post)

@main.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit(id):
	post = Post.query.get_or_404(id)
	if current_user != post.author:
		abort(403)
	form = PostForm()
	if form.validate_on_submit():
		post.title = form.title.data
		post.body = form.body.data
		db.session.add(post)
		db.session.commit()
		flash('The post has been updated.')
		return redirect(url_for('.post', id=post.id))
	form.title.data = post.title
	form.body.data = post.body
	return render_template('edit_post.html', form=form)

@main.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete(id):
	posts = Post.query.filter_by(id=id)
	if not posts:
		abort(404)
	for post in posts:
		if post.author != current_user:
			abort(403)
	posts.delete()
	db.session.commit()
	return redirect(url_for('.index'))

@main.route('/delete_user/', methods=['GET', 'POST'])
@login_required
def delete_user():
	form = DeleteUserForm()
	if form.validate_on_submit():
		users = User.query.filter_by(id=current_user.id)
		posts = Post.query.filter_by(author_id=current_user.id)
		logout_user()
		if not users:
			abort(404)
		posts.delete()
		users.delete()
		db.session.commit()
		return redirect(url_for('.index'))
	return render_template('delete_user.html', form=form)

@main.route('/users')
def users():
	users = User.query.all()
	post_counts = dict()
	for user in users:
		posts = Post.query.filter_by(author_id = user.id)
		post_counts[user.id] = posts.count()
	return render_template('users.html', users=users, post_counts=post_counts)

@main.route('/posts_by/<int:id>')
def posts_by(id):
	user = User.query.get_or_404(id)
	posts = Post.query.filter_by(author_id = user.id)
	return render_template('posts_by.html', user=user, posts=posts)

