from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask_login import UserMixin
from flask import current_app
from . import db, login_manager
from datetime import datetime
import bleach
from markdown import markdown


class User(UserMixin, db.Model):
	__tablename__ = 'user'
	id = db.Column(db.Integer, primary_key = True)
	username = db.Column(db.String(256), unique = True, index = True)
	password_hash = db.Column(db.String(128))
	email = db.Column(db.String(256), unique = True, index = True)
	posts = db.relationship('Post', backref='author', lazy='dynamic')
	confirmed = db.Column(db.Boolean, default=False)

	@property
	def password(self):
		raise AttributeError()

	@password.setter
	def password(self, password):
		self.password_hash = generate_password_hash(password)

	def get_id(self):
		return self.id

	def verify_password(self, password):
		return check_password_hash(self.password_hash, password)

	def generate_confirmation_token(self, expiration=3600):
		s = Serializer(current_app.config['SECRET_KEY'], expiration)
		return s.dumps({'confirm': self.id}).decode('utf-8')

	def confirm(self, token):
		s = Serializer(current_app.config['SECRET_KEY'])
		try:
			data = s.loads(token.encode('utf-8'))
		except:
			return False
		if data.get('confirm') != self.id:
			return False
		self.confirmed = True
		db.session.add(self)
		return True

	def __repr__(self):
		return '<User %r>' % self.username


class Post(db.Model):
	__tablename__ = 'post'
	id = db.Column(db.Integer, primary_key = True)
	title = db.Column(db.String(256), index = True)
	body = db.Column(db.Text)
	body_html = db.Column(db.Text)
	timestamp = db.Column(db.DateTime, index = True, default = datetime.utcnow)
	author_id = db.Column(db.Integer, db.ForeignKey('user.id'))

	@staticmethod
	def on_changed_body(target, value, oldvalue, initiator):
		allowed_tags = ['a', 'abbr', 'acronym', 'b', 'blockquote', 'code',
						'em', 'i', 'li', 'ol', 'pre', 'strong', 'ul',
						'h1', 'h2', 'h3', 'p']
		target.body_html = bleach.linkify(bleach.clean(
			markdown(value, output_format='html'),
			tags=allowed_tags, strip=True))

db.event.listen(Post.body, 'set', Post.on_changed_body)

@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))
