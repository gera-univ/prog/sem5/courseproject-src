import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
	MAIL_SERVER = os.environ.get('MAIL_SERVER', 'mail.spiralarms.org')
	MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
	MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'true').lower() in \
		['true', 'on', '1']
	MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
	MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
	MAIL_SUBJECT_PREFIX = '[Account Registration]'
	MAIL_SENDER = 'Herman <herman@spiralarms.org>'

	SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SECRET_KEY = os.environ.get('SECRET_KEY') or 'change me'

	@staticmethod
	def init_app(app):
		pass

config = {
	'default' : Config
}
